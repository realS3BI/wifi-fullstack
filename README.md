# React App Projekt MERN011

## Vorraussetzungen

-  MongoDB
-  React
-  Nodejs server
-  Authentifizierung (CRUD Operations)

## Vorschläge

-  Tournament-Manager
-  Quiz-App (Spieleshow)
-  Mitarbeiterverwaltung

## Ausführungen

### _Tournament-Manager_

-  Usermanager über MongoDB
-  Loginscreen (Register, Password Forgot, ...)
-  Einzelne Screens für (OBS-)Szenen (Verschiedene Themes für unterschiedliche "Kunden")
   -- /view/tm/tmgl/season1/driver/...
   -- /view/csgo/eslm/pre-game/...
-  Datamanager
-  API Einbindung (https://blog.faceit.com/launching-the-faceit-developer-portal-5740fb48ac26)
<!-- - Infoscreen für Nutzer und "Einbinder" -->
-  ev. Websocket, um Events zu steuern

### _Quiz-App (Spieleshow)_

-  Usermanager über MongoDB
-  Spielemanager über MongoDB oder CMS (Storyblok)
-  Loginscreen (Register, Password Forgot, ...)
-  Spieleoverlay (Websockets)
   -- Verschiedene Spieloverlays
   -- ev Websocketmultiplayer
   -- Adminoverlay

### _Mitarbeiterverwaltung_

> Dienstplanprogramm?
> MA-Verwaltung?

-  Usermanager über MongoDB
-  Loginscreen (Register, Password Forgot, ...)
-  MA Bereich
   -- Infos anpinnen
   -- Nachrichten/Mails schreiben
   -- To-Dos
-  Admin Bereich
   -- Mails an MAs schreiben
   -- ...
