const express = require('express');
const app = express();
const cors = require('cors');
const PORT = process.env.PORT || 3000
const fs = require('fs');
const crypto = require('crypto');
const { v4: uuidv4 } = require('uuid');


app.use(express.static('www'))
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(cors())


const API_PATH = '/data';
const USERS = __dirname + '/daten/users.json';

const userJSON = fs.readFileSync(USERS, 'utf8');


app.get(API_PATH, (req, res) => {
	res.send(userJSON)
})

app.post(API_PATH, (req, res) => {
	let users = JSON.parse(userJSON);
	const hashpwd = hash(req.body.Password)

	let newUser = {
		ID: uuidv4(),
		Name: req.body.Name,
		Username: req.body.Username,
		Password: hashpwd
	};
	users.push(newUser);
	fs.writeFile(USERS, JSON.stringify(users), err => {
		res.send(newUser);
	});
})

app.put(API_PATH, (req, res) => {
	const parsedUsers = JSON.parse(userJSON)
	let thisUser = parsedUsers.find(x => x.ID == req.body.ID)
	thisUser = req.body
	newUsers = parsedUsers.filter(x => x.ID != req.body.ID)
	newUsers.push(thisUser)

	fs.writeFileSync(USERS, JSON.stringify(newUsers))
	res.send('Erfolgreiches Update durchgeführt')
})

app.delete(API_PATH, (req, res) => {
	const parsedUsers = JSON.parse(userJSON)
	const userLeft = parsedUsers.filter(x => x.ID != req.body.ID)
	fs.writeFileSync(USERS, JSON.stringify(userLeft))
	res.send('Wurde gelöscht!')
})

app.listen(PORT, (err) => {
	if (err) console.log("Error in server setup")
	console.log("Server listening on Port", PORT);
})


const hash = (pwd) => {
	let key = '12345678912345678912345678912345';
	let iv = crypto.randomBytes(16);

	let mykey = crypto.createCipheriv('aes-256-cbc', key, iv);
	let encrypted = mykey.update(pwd, 'utf-8', 'hex')
	encrypted += mykey.final('hex')
	return encrypted
}
