// TO-DO: 
// Handling wenn keine Daten vorhanden sind!
// Schema automatisch holen!

const table = document.getElementById('table')
var schema = ['Name', 'Username', 'Password', 'ID']
const API_PATH = '/data'

// CRUD
const get = () => {
	fetch(API_PATH, {
		method: 'GET',
		headers: { 'Content-Type': 'application/json' }
	}).then(res => {
		return res.json()
	}).then(data => {
		tableCreate(table, data);
	})
}

const submit = () => {
	if (checkForm(true) == '') {
		return
	}
	let data = checkForm(true);

	fetch(API_PATH, {
		method: 'POST',
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
		body: new URLSearchParams(data)
	}).then(response => {
		return response.json()
	}).then(data => {
		addRow(data);
	})
};

const update = () => {
	if (checkForm(false) == '') {
		return
	}
	let data = checkForm(false);
	reset();
	fetch(API_PATH, {
		method: 'PUT',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify(data)
	})
		.then(response => response.text())
		.then(data => {
			document.getElementById('notconfirm').innerHTML = data.toString();
			const removeChilds = (parent) => {
				while (parent.lastChild) {
					parent.removeChild(parent.lastChild);
				}
			};
			removeChilds(table);
			// FIX SYNC
			setTimeout(() => {
				get()
			}, 1000);
		});
}

const del = () => {
	let data = checkForm(false);
	reset();

	fetch(API_PATH, {
		method: 'DELETE',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify(data)
	})
		.then(response => response.text())
		.then(data => {
			document.getElementById('notconfirm').innerHTML = data.toString();
			const removeChilds = (parent) => {
				while (parent.lastChild) {
					parent.removeChild(parent.lastChild);
				}
			};
			removeChilds(table);
			// FIX SYNC
			setTimeout(() => {
				get()
			}, 1000);
		});
}

const tableCreate = (table, data) => {
	const keys = Object.keys(data[0]);
	const genTableHead = (table, data) => {
		let thead = table.createTHead();
		let row = thead.insertRow();

		let th, text
		th = document.createElement("th");
		text = document.createTextNode('EDIT');
		th.appendChild(text);
		row.appendChild(th);

		th = document.createElement("th");
		text = document.createTextNode('Delete');
		th.appendChild(text);
		row.appendChild(th);
		for (let key of data) {
			let th = document.createElement("th");
			let text = document.createTextNode(key);
			th.appendChild(text);
			row.appendChild(th);
		}
	}
	const genTable = (table, data) => {
		let tbody = table.createTBody();
		for (let element of data) {
			var row = tbody.insertRow();
			addButtons(row)
			for (key in element) {
				let innerCell = row.insertCell();
				let text = document.createTextNode(element[key]);
				innerCell.appendChild(text);
			}
		}
	}

	genTableHead(table, keys);
	genTable(table, data);
}

const addRow = (data) => {
	var tbody = document.getElementsByTagName('tbody')[0];

	var row = tbody.insertRow();
	addButtons(row)

	for (let index = 0; index < schema.length; index++) {
		const element = data[schema[index]];
		var innerCell = row.insertCell();
		var newText = document.createTextNode(element);
		innerCell.appendChild(newText);
	}
	for (let index = 0; index < document.querySelectorAll('input').length; index++) {
		const element = document.querySelectorAll('input')[index];
		element.value = ''
	}
}

const showPassword = () => {
	var el1 = document.querySelector("input[name=password]");
	var el2 = document.querySelector("input[name=confirm]");
	var el = [el1, el2]
	for (var i = 0; i < el.length; i++) {
		if (el[i].type === "password") {
			el[i].type = "text";
		} else {
			el[i].type = "password";
		}
	}
}

const checkForm = (password) => {
	let data = {};
	data.Name = document.querySelector("input[name=name]").value;
	data.Username = document.querySelector("input[name=username]").value;
	data.Password = document.querySelector("input[name=password]").value;
	if (password) {
		if (data.password == '' || document.querySelector("input[name=confirm]").value == '') {
			document.getElementById('notconfirm').innerHTML = 'Passwort eingeben!'
			return ''
		} else {
			document.getElementById('notconfirm').innerHTML = ''
		}
		if (document.querySelector("input[name=password]").value !== document.querySelector("input[name=confirm]").value) {
			document.getElementById('notconfirm').innerHTML = 'Passwörter stimmen nicht überein!'
			return ''
		} else {
			document.getElementById('notconfirm').innerHTML = ''
		}
	} else {
		data.ID = document.getElementById('showID').innerHTML.split(':')[1]
	}
	return data
}

const fill = (row) => {
	// 0 und 1 sind für für Buttons
	document.querySelector("input[name=name]").value = row.childNodes[2].textContent
	document.querySelector("input[name=username]").value = row.childNodes[3].textContent
	document.querySelector("input[name=password]").value = row.childNodes[4].textContent
	document.getElementById('showID').innerHTML = 'ID:' + row.childNodes[5].textContent
	document.getElementById('passwords').style.display = 'none'
	const array = document.querySelectorAll('#showPassword')
	for (let index = 0; index < array.length; index++) {
		array[index].style.display = 'none';
	}
	document.getElementById('submit').style.display = 'none'
}
const reset = () => {
	document.querySelector("input[name=name]").value = ''
	document.querySelector("input[name=username]").value = ''
	document.querySelector("input[name=password]").value = ''
	document.getElementById('showID').innerHTML = ''
	document.getElementById('passwords').style.display = 'flex'
	const array = document.querySelectorAll('#showPassword')
	for (let index = 0; index < array.length; index++) {
		array[index].style.display = 'block';
	}
	document.getElementById('submit').style.display = 'block'
	document.getElementById('update').style.display = 'none'
	document.getElementById('delete').style.display = 'none'
}

const addButtons = (row) => {
	let cell = row.insertCell();
	var para = document.createElement('button');
	para.innerHTML = 'Edit!';
	para.classList.value = 'w-100 p-2';
	para.onclick = function () {
		fill(row)
		document.getElementById('update').style.display = 'flex'
	};
	cell.appendChild(para);

	let newCell = row.insertCell();
	var newPara = document.createElement('button');
	newPara.innerHTML = 'Delete!';
	newPara.classList.value = 'w-100 p-2';
	newPara.onclick = function () {
		fill(row)
		document.getElementById('delete').style.display = 'flex'
	};
	newCell.appendChild(newPara);
}


// Get Database
get()