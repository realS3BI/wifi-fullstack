const http = require('http');
const fs = require('fs');
const helpers = require('./helpers');

const PORT = process.env.PORT || 3500;

http.createServer((req, res) => {

	console.log('Request', req.url, req.method);
	// req.url == Datei, '/' => 'index.html'
	switch (req.url) {
		case '/':
			req.url = '/index.html';
		case '/index.html':
		case '/style.css':
		case '/client.js':
			helpers.sendFile(res, req.url.substring(1));
			break;
		case '/data':
			console.log(req.query);
			break
		default:
			res.writeHead(404)
			res.end('');
	}

}).listen(PORT);
console.log(`Server running :${PORT}`);