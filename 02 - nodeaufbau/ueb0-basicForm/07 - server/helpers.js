const fs = require( 'fs' );
const path = require( 'path' );

const resHead = (res,code,filename)=>{
    let type;
    //let ext = filename.substr( filename.lastIndexOf('.')+1 );
    switch( path.extname(filename).substring(1) ) {
        case 'html': type = 'text/html'; break;
        case 'css': type = 'text/css'; break;
        case 'js': type = 'text/javascript'; break;
        case 'json': type = 'application/json'; break;
        default: type = 'text/plain';
    }
    res.writeHead( code, {'Content-Type': type+' ,charset=utf-8'} );
}

const resHeadReturn = (filename)=>{
    let type;
    let ext = filename.substr( filename.lastIndexOf('.')+1 );
    switch( ext ) {
        case 'html': type = 'text/html'; break;
        case 'css': type = 'text/css'; break;
        case 'js': type = 'text/javascript'; break;
        case 'json': type = 'application/json'; break;
        default: type = 'text/plain';
    }
    return {'Content-Type': type+' ,charset=utf-8'};
}

const sendFile = (res, filename) => {
    fs.readFile( './static/'+filename, (err,data)=>{
        if ( err ) {
            res.writeHead( 404 );
            res.end( '' );
        } else {
            resHead(res,200,filename);
            res.end( data );
        }
    } );
}

module.exports = {
    resHead,
    resHeadReturn,
    sendFile
}