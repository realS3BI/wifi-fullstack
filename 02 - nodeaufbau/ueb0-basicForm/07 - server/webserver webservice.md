// Webservice 
// keine statischen Inhalte
// Datenverwaltung, -verarbeitung
// CRUD - create read update delete
// RESTful CRUD (Methoden: GET, POST, PUT, PATCH, DELETE)
// GET -> read
// POST -> create
// DELETE -> delete
// PUT -> update all data
// PATCH -> update single/multi data

// Webserver (Apache, NGINX, IIS)
// routet, kümmert sich um Anfragen
// HTML, Ressourcen ausliefere (statisch)
// + dynamische Inhalte (Verknüpfung Datenbank)