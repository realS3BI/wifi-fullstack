const express = require('express');
const app = express();
const { Server } = require("socket.io");
const server = app.listen(3000, console.log('Server läuft auf 3000'))

const fs = require('fs');
const path = require('path');
const quiz = JSON.parse(fs.readFileSync(path.join(__dirname, 'www', 'fragen_all.json'))).quiz

const io = new Server(server)

app.use(express.static('www'))
app.use(express.urlencoded({ extended: false }));
app.use(express.json());


io.on('connection', (socket) => {
	console.log(`User ${socket.id} ist verbunden`);

	socket.on('newQ', (cq) => {
		newQ(cq, socket)
	})
	socket.on('checkAnswer', (answer, answerID, cb) => {
		cb(answer == answerID)
	})
})

const newQ = (cq, socket) => {
	let thisQ = quiz[Math.floor(Math.random() * quiz.length)]
	if (cq != thisQ.f) {
		socket.emit('loadQ', thisQ)
	} else {
		newQ(cq, socket)
	}
}