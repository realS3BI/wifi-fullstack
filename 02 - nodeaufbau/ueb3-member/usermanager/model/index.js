import fs from 'fs';
import path from 'path';
import { nanoid } from 'nanoid';
import bcrypt from 'bcrypt';

const USERS = path.resolve() + '/usermanager/model/users.json'

const getUser = async () => {
	let users = await fs.promises.readFile(USERS);
	return JSON.parse(users);
}

const newUser = async (data) => {
	let users = await getUser();
	users.push({
		id: nanoid(),
		username: data.username,
		email: data.email,
		password: await bcrypt.hash(data.password, 12)
	});
	await fs.promises.writeFile(USERS, JSON.stringify(users));
	return users;
}

const checkUser = async (data) => {
	let users = await getUser();
	let user = users.find(user => { return user.username == data.username })
	let loginOK;
	if (user != undefined) {
		await bcrypt.compare(data.password, user.password).then((res) => { loginOK = res })
	} else { loginOK = false }
	return loginOK;
}

export default { getUser, newUser, checkUser };