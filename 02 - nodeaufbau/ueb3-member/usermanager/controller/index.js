import view from '../view/index.js';
import model from '../model/index.js';


const responseHTML = async (req, res) => {
	let html = await view.loadHTML(req.url)
	res.send(html);
}

const checkUser = async (req, res) => {
	let loginOK = await model.checkUser(req.body)
	if (loginOK) {
		res.redirect('/member')
	} else {
		res.redirect('/user/failed')
	}
}

const addUser = async (req, res) => {
	if (req.body.password === req.body.confirm) {
		await model.newUser(req.body);
		res.redirect('/user');
	} else res.redirect('/user/failed')
}

export default { responseHTML, addUser, checkUser };