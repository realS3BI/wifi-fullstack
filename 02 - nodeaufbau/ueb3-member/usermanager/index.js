import express from 'express';
const r = express.Router();

r.use('/static', express.static('usermanager/static'))

import controller from './controller/index.js';

r.get('/', (req, res) => res.redirect('/user/login'))
r.get('/login', controller.responseHTML);
r.get('/login', controller.responseHTML);
r.get('/failed', controller.responseHTML);
r.get('/signup', controller.responseHTML);

r.post('/login', controller.checkUser);
r.post('/signup', controller.addUser);

export default r;