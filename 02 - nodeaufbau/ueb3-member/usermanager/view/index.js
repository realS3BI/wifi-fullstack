import fs from 'fs';
import path from 'path';

const loadHTML = async (requrl) => {
	let file = await fs.promises.readFile(path.join(path.resolve(), 'usermanager/view/', requrl + '.hbs'), 'utf8', (err, data) => {
		if (err) { throw err }
		return data
	});
	return file
}

export default { loadHTML }