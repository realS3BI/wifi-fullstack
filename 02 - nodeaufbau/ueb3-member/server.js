import express from 'express';
const app = express();
const server = app.listen(process.env.PORT || 3000, () => {
	console.log('Express on!');
})
app.use( express.urlencoded( {extended:false }))

import usermanager from './usermanager/index.js'
app.use('/user', usermanager)
import memberpage from './memberpage/index.js'
app.use('/member', memberpage)

app.get('/', (req, res) => res.redirect('/user/login'))

import hbs from 'express-hbs';
app.engine( 'hbs', hbs.express4() );
app.set( 'view engine', 'hbs' );