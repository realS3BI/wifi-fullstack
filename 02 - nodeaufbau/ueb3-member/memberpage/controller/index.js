// import fs from 'fs';
import path from 'path';

// import view from '../view/index.js';
// import model from '../model/index.js';

const responseHTML = async (req, res) => {
	res.render(path.resolve() + '/memberpage/view/index');
}

export default { responseHTML };