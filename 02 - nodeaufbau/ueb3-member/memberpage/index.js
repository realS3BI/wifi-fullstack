import express from 'express';
const r = express.Router();

import controller from './controller/index.js';

r.get('/', controller.responseHTML);

export default r;