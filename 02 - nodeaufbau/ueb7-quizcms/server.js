const dotenv = require('dotenv').config();
const express = require('express')
const nano = require('nano');
const { Server } = require("socket.io");
const app = express();
const server = app.listen(process.env.PORT || 3000, () => {
	console.log('Express on');
});
const io = new Server(server)


app.use(express.static('www'));
app.use(express.urlencoded({ extended: false }));


const fs = require('fs');
const path = require('path');
const quiz = JSON.parse(fs.readFileSync(path.join(__dirname, 'www', 'fragen_all.json'))).quiz




const DBUSER = process.env.DBUSER;
const DBPWD = process.env.DBPWD;
const COUCHDB_URL = `http://${DBUSER}:${DBPWD}@localhost:5984`;

const couch = nano(COUCHDB_URL);
const dbName = 'quiz'

couch.db.create(dbName, (err) => {
	if (err && err.statusCode != 412) {
		console.error('dbcreate Error', err);
	}
	else {
		console.log(`database "${dbName}" exists`);
	}
});

const db = couch.use(dbName);




io.on('connection', (socket) => {
	console.log(`User ${socket.id} ist verbunden`);

	socket.on('createQuiz', () => {
		console.log('wurde bereits hinzugefügt!');
		for (let i = 0; i < quiz.length; i++) {
			const el = quiz[i];
			db.insert({ f: el.f, a: el.a, c: el.c })
		}
	})

	socket.on('newQ', (cq) => {
		console.log(db.list().then((all) => { console.log(all.rows); }));
		newQ(cq, socket)
	})
	socket.on('checkAnswer', (answer, answerID, cb) => {
		cb(answer == answerID)
	})
	socket.on('insertQ', (question) => {
		db.insert(question)
	})
})



app.get('/questions', (req, res) => {
	db.find({ selector: {}, limit: 100 }).then(db => res.send(db.docs))
})

const newQ = (cq, socket) => {
	db.list().then((all) => { console.log(all.rows); })
	let thisQ = quiz[Math.floor(Math.random() * quiz.length)]
	if (cq != thisQ.f) {
		socket.emit('loadQ', thisQ)
	} else {
		newQ(cq, socket)
	}
}