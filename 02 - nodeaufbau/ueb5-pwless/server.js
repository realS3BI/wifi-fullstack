const express = require('express')
const session = require('express-session')
const socket = require('socket.io')
const nodemailer = require("nodemailer");
require('dotenv').config()
const path = require('path')
const fs = require('fs')

const PORT = process.env.PORT || 3000
const app = express();
const server = app.listen(PORT, () => console.log(`Listening to port ${PORT}.`));
const io = socket(server);

// Übertragen von ${server} in eine Middleware??

app.set('trust proxy', 1);
app.use(session({
	secret: 'random string fuer hash',
	resave: false,
	saveUninitialized: true,
	cookie: { secure: false }
}));

const users = JSON.parse(fs.readFileSync(path.join(__dirname, 'json', 'users.json'), 'utf8'));

io.on('connection', (socket) => {
	socket.on('signup', (user, mail) => {
		const newUser = { u: user, mail: mail }
		if (typeof users.find(v => v.u == newUser.u) !== 'undefined' || typeof users.find(v => v.mail === newUser.mail) !== 'undefined') {
			socket.emit('signup-fail')
			return
		}
		users.push(newUser);

		fs.writeFile(path.join(__dirname, 'json', 'users.json'), JSON.stringify(users), (err) => {
			if (err) {
				console.log(err, 'error');
			}
		})
	})
	socket.on("login", (user) => {
		const thisUser = users.find(u => u.u === user)
		if (thisUser) {
			mail(thisUser, socket.id) //check if Mail works properly!
			socket.join(socket.id)
		} else { socket.emit("invalid"); }
	});
});


const mail = (user, socketID) => {
	var transporter = nodemailer.createTransport({
		service: 'gmail',
		auth: {
			user: process.env.NODEJS_BASIS_GMAIL_USER,
			pass: process.env.NODEJS_BASIS_GMAIL_PASS
		}
	});

	const mailOptions = {
		from: 'testnodejssebi@gmail.com',
		to: user.mail,
		subject: 'test mail html',
		html: `<a href="http://localhost:${PORT}/mail.html?socketID=${socketID}&username=${user.u}" target="_blank">Login</a>`
	};

	transporter.sendMail(mailOptions, (err, info) => {
		if (err)
			console.log(err)
		else
			console.log(info);
	})
}


app.post('/confirm', (req, res) => {
	io.to(req.body.socketID).emit('confirm', req.body.username)
	console.log(req.body);
	res.send({ username: req.body.username })
})


app.post('/loginSession', (req, res) => {
	if (users.find(v => v.user === req.body.username)) {
		req.session.eingeloggt = true;
		req.session.username = req.body.username;
		console.log(req.session.id);
		res.redirect('/members');
	}
	else {
		res.end('NOK');
	}
})

app.get('/logout', (req, res) => {
	req.session.destroy(
		() => { res.redirect('/login.html'); }
	)
});

app.use(express.urlencoded({ extended: false }));
app.use(express.static('www'));

app.use('/members', (req, res, next) => {
	if (req.session.eingeloggt) {
		next();
	} else {
		res.redirect('/login.html');
	}
})
app.use('/members', express.static('www_members'));
