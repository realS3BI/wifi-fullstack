import express from 'express';
import session from 'express-session';
import pp from 'passport';
import Local from 'passport-local';
import path from 'path';

const app = express();
const server = app.listen(process.env.PORT || 3000, () => {
	console.log('Express on!');
})

const users = [{ u: 'admin', p: 'admin' }];

pp.serializeUser((user, done) => { done(null, user); });
pp.deserializeUser((user, done) => { done(null, user); });

pp.use(new Local({
	usernameField: 'username',
	passwordField: 'password'
}, (username, password, done) => {
	// check mit user.json od. Datenbanken, etc.
	if (users.find(v => {
		return v.u === username && v.p === password
	})) {
		done(null, { username: username, level: 'normal' }); // Info zum User
	} else {
		done(null);
	}
}));

app.use(session({ // Express-Session
	secret: 'geheim',
	resave: false,
	saveUninitialized: false
}));

app.use(pp.initialize());
app.use(pp.session());

app.use(express.json())

app.use(express.static('www'));

app.post('/login', pp.authenticate('local'), (req, res) => {
	console.log(req.sessionID);
	res.send({ status: 'ok' })
})

const checkAuth = (req, res, next) => {
	if (req.user) {
		console.log('test');
		next();
	} else {
		res.end('hier gibts nix zu sehen...');
	}
}

app.use(checkAuth);
//ab hier nur eingeloggte

app.get('/logout', (req, res) => {
	req.logout(); // Passport
	console.log(req.user);
	res.redirect('/www/login.html');
})