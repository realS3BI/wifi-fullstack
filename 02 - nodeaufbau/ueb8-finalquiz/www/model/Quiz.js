const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const QuizSchema = new Schema({
	f: {
		type: String,
		required: true,
		unique: true,
	},
	a: {
		type: Array,
		required: true
	},
	c: {
		type: Number,
		required: true
	}
});

module.exports = mongoose.model('Quiz', QuizSchema);