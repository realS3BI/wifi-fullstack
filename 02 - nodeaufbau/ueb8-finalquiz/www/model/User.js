const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
	username: {
		type: String,
		required: true,
		unique: true,
	},
	email: {
		type: String,
		required: true,
		unique: true
	},
	password: {
		type: String,
		required: true
	},
	level: {
		type: String,
		default: 'standard',
	}
});


UserSchema.pre('save', async function (next) {
	const user = this;
	if (user.isNew || user.isModified('password')) { // nur wenn neu od. Passwort ändern
		let salt = await bcrypt.genSalt(10);
		let hashedPass = await bcrypt.hash(user.password, salt);
		user.password = hashedPass;
	}
	next();
});

module.exports = mongoose.model('User', UserSchema);