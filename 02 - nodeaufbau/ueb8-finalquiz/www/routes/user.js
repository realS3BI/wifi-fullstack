const express = require('express');
const r = express.Router();

const controller = require('../controller/user')

r.get('/register', controller.responseHTML)
r.get('/login', controller.responseHTML)
// r.get('/admin', controller.responseAdmin)
r.get('/admin', controller.responseHTML)

// r.get('/getTable', (req, res) => {
// 	res.send([{ name: 'user', email: 'mail' }, { name: 'admin', email: 'adminmail'}]);
// })
r.get('/getTable', controller.getUsertable)

r.post('/register', controller.register)
r.post('/login', controller.login)
// r.post('/login-oauth', controller.oauthLogin)

module.exports = r