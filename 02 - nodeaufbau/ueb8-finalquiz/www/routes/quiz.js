const express = require('express');
const r = express.Router();

const controller = require('../controller/quiz')

r.get('/', controller.responseHTML)
r.get('/admin', controller.responseAuthor)

module.exports = r