const express = require('express');
const r = express.Router();

const controller = require('../controller/app')

r.get('/', (req, res) => {
	if (req.user) {
		if (req.user.level === 'admin') {
			res.redirect('/user/admin')
		} else if (req.user.level === 'author') {
			res.redirect('/quiz/admin')
		} else {
			res.redirect('/quiz')
		}
	} else {
		res.redirect('/user/login')
	}
})
r.get('/login', (req, res) => res.redirect('/user/login'))
r.get('/register', (req, res) => res.redirect('/user/register'))
r.get('/logout', (req, res) => {
	req.session.destroy(
		() => { res.redirect('/'); }
	)
});
r.get('/error', controller.responseHTML)

module.exports = r