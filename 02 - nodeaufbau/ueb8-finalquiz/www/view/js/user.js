const table = document.getElementById('table')

fetch('getTable/', {
	method: 'GET',
	headers: { 'Content-Type': 'application/json' }
}).then(res => {
	return res.json()
}).then(data => {
	Helpers.tableCreate(table, data);
})

$('#here_table').append(table);