const alertClasses = 'flex flex-col space-y-1 rounded text-white font-bold px-5 py-2 shadow transition-colors text-center'
const timeoutTime = 1000;

$("#register").click((e) => {
	e.preventDefault();
	const confirm = $("#confirm").val();
	const user = Helpers.getData();
	if (user.password !== confirm) {
		Helpers.alert('Passwörter stimmen nicht überein!')
		$('#alertMessage').addClass(alertClasses)
		$('#alertMessage').addClass('bg-red-600')
		return
	}
	fetch('register/', {
		method: 'POST',
		headers: { "Content-Type": "application/json" },
		body: JSON.stringify(user),
	})
		.then((res) => {
			return res.json();
		})
		.then((data) => {
			Helpers.alert(data.name)
			$('#alertMessage').addClass(alertClasses)
			if (data.user) {
				$('#alertMessage').addClass('bg-lime-600')
				$("#username").prop('disabled', true);
				$("#email").prop('disabled', true);
				$("#password").prop('disabled', true);
				$("#confirm").prop('disabled', true);
				$("#register").prop('disabled', true);
				$("#register").removeClass('hover:bg-blue-700');
				setTimeout(() => {
					$(location).attr('href', '/user/login');
				}, timeoutTime);
			} else {
				$('#alertMessage').addClass('bg-red-600')
			}
		});
})

$('#login').click((e) => {
	e.preventDefault();
	const user = Helpers.getData();
	fetch('login/', {
		method: 'POST',
		headers: { "Content-Type": "application/json" },
		body: JSON.stringify(user),
	})
		.then((res) => {
			return res.json();
		}).then((data) => {
			Helpers.alert(data.name)
			$('#alertMessage').addClass(alertClasses)
			if (data.user) {
				$('#alertMessage').addClass('bg-lime-600')
				$("#username").prop('disabled', true);
				$("#password").prop('disabled', true);
				$("#login").prop('disabled', true);
				$("#login").removeClass('hover:bg-blue-700');
				setTimeout(() => {
					if (data.user.level === 'admin') {
						$(location).attr('href', '/user/admin');
					} else if (data.user.level === 'author') {
						$(location).attr('href', '/quiz/admin');
					} else {
						$(location).attr('href', '/quiz');
					}
				}, timeoutTime);
			} else {
				$('#alertMessage').addClass('bg-red-600')
			}
		})
})
