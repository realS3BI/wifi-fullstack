class Helpers {
	static alert(message) {
		let d = document.createElement('div');
		d.id = 'alertMessage';
		d.innerHTML = message;
		let box = $('#innerBox')
		let old = $('#alertMessage')
		if (old) {
			old.remove()
		}
		box.append(d)
	}
	static getData() {
		const username = $("#username").val();
		const email = $("#email").val();
		const password = $("#password").val();
		const csrf = $("#csrf").val();
		const user = {
			username: username,
			email: email,
			password: password,
			csrf: csrf,
		};
		return user
	}
	static tableCreate = (table, data) => {
		var newUsers = []
		for (let i = 0; i < data.length; i++) {
			const el = data[i];
			const newEl = { username: el.username, email: el.email, level: el.level }
			newUsers.push(newEl)
		}
		const keys = Object.keys(newUsers[0])

		// genHeader
		const thead = $('<thead></thead>')
		$('table').append(thead)
		$('thead').addClass('bg-white border-b')

		const rowHead = $('<tr></tr>')
		thead.append(rowHead)

		// let th, text
		// th = document.createElement("th");
		// text = document.createTextNode('EDIT');
		// th.appendChild(text);
		// rowHead.appendChild(th);

		// th = document.createElement("th");
		// text = document.createTextNode('Delete');
		// th.appendChild(text);
		// rowHead.appendChild(th);

		const th = $('<th></th>')
		th.addClass('text-sm font-medium text-gray-900 px-6 py-4 text-left').text('NR.')
		rowHead.append(th)

		for (let key of keys) {
			const th = $('<th></th>')
			key = key.toLowerCase().replace(/\b[a-z]/g, (letter) => {
				return letter.toUpperCase();
			});
			th.addClass('text-sm font-medium text-gray-900 px-6 py-4 text-left').text(key)
			rowHead.append(th);
		}
		const buttonTh = $('<button></button>')
		rowHead.append(buttonTh)

		// genBody
		const tbody = $('<tbody></tbody>')
		$('table').append(tbody)
		for (let [i, thisUser] of newUsers.entries()) {
			const row = $('<tr></tr>')
			row.addClass('border-b')
			if (i % 2 === 0) {
				row.addClass('bg-gray-100')
			} else {
				row.addClass('bg-white')
			}
			tbody.append(row)

			const td = $('<td></td>')
			td.addClass('px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900').text(`#${i + 1}`)
			row.append(td)

			const el = Object.values(thisUser);
			for (let v of el) {
				const td = $('<td></td>')
				td.addClass('text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap').text(v)
				row.append(td)
			}

			const buttonTd = $('<td></td>')
			buttonTd.addClass('py-4')
			const button = $('<button></button>')
			button.addClass('bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow')
				.text('Edit')
				.click((e) => console.log(e.target))
			buttonTd.append(button)
			row.append(buttonTd)
		}
	}
}
