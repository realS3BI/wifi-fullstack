const fs = require('fs');
const path = require('path');

const loadHTML = async (requrl) => {
	requrl == '/' ? requrl = '/index' : requrl = requrl
	let file = await fs.promises.readFile(path.resolve('www/view/static/quiz' + requrl + '.html'), 'utf8', (err, data) => {
		if (err) { throw err }
		return data
	});
	return file
}

module.exports = { loadHTML }