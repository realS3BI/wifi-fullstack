const view = require('../view/static/app');

const responseHTML = async (req, res) => {
	let html = await view.loadHTML(req.url)
	res.send(html);
}

module.exports = {
	responseHTML
}