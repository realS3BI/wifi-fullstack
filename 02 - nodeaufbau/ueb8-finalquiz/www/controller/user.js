const User = require('../model/User') //mongoose Schema
const view = require('../view/static/user')

const pp = require('passport')

const responseHTML = async (req, res) => {
	let html = await view.loadHTML(req.url)
	res.send(html);
}

const responseAdmin = async (req, res) => {
	if (req.user && req.user.level === 'admin') {
		let html = await view.loadHTML(req.url)
		res.send(html);
	}
	else {
		res.redirect('/')
	}
}

const register = (req, res) => {
	const user = {
		username: req.body.username,
		email: req.body.email,
		password: req.body.password
	}
	User.create(user)
		.then(() => {
			res.send({ name: 'Register successfully!', user: user })
		}).catch(() => {
			res.send({ name: 'User or Mail already taken!' })
		})
}

const login = (req, res) => {
	pp.authenticate('local', (error, user, info) => {
		req.login(user, (err) => {
			if (!err) {
				res.send({ name: 'Logged in successfully!', user: user });
			} else {
				res.send({ name: 'Password is wrong!' });
			}
		});
	})(req, res)
}

// const oauthLogin = (req, res) => {
// 	pp.authenticate('oauth2', (error, user, info) => {
// 		req.login(user, (err) => {
// 			if (!err) {
// 				res.send({ name: 'Logged in successfully!', user: user });
// 			} else {
// 				res.send({ name: 'Password is wrong!' });
// 			}
// 		});
// 	})(req, res)
// }

const getUsertable = async (req, res) => {
	const users = await User.find({})
	var newUsers = []
	for (let i = 0; i < users.length; i++) {
		const el = users[i];
		const newEl = { id: el._id, username: el.username, email: el.email, level: el.level }
		newUsers.push(newEl)
	}
	res.send(newUsers)
}

module.exports = {
	responseHTML,
	responseAdmin,
	register,
	login,
	// oauthLogin
	getUsertable
}