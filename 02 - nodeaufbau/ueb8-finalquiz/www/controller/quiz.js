const view = require('../view/static/quiz')

const responseHTML = async (req, res) => {
	let html = await view.loadHTML(req.url)
	res.send(html);
}

const responseAuthor = async (req, res) => {
	if (req.user.level === 'admin' || req.user.level === 'author') {
		let html = await view.loadHTML(req.url)
		res.send(html);
	} else {
		res.redirect('/')
	}
}

module.exports = {
	responseHTML,
	responseAuthor
}