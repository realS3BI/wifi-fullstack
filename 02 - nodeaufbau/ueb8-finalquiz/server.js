const express = require('express');
const app = express();
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => { console.log(`Server running. Port: ${PORT}`) });


// MONGODB
const mongoose = require('mongoose');
const mongo_uri = `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASS}@${process.env.MONGO_CLUSTER}/${process.env.MONGO_DB}?retryWrites=true&w=majority`
mongoose.connect(mongo_uri).then(async () => {
	console.log('Database connected');
}).catch(err => {
	console.log('Database not connected' + err);
})


// Favivon
const favivon = require('serve-favicon');
app.use(favivon('favicon.ico'))


// Security Helpers
const cors = require('cors');
app.use(cors())

const helmet = require('helmet');
app.use(
	helmet.contentSecurityPolicy({
		directives: {
			"script-src": ["'self'", "cdn.tailwindcss.com", "code.jquery.com"],
		}
	})
);

var cookieParser = require('cookie-parser');
app.use(cookieParser());

// const csrf = require('csurf');
// var csrfProtection = csrf({ cookie: true })
// app.use( csrfProtection, (err,req,res,next) => {
//     if (err.code !== 'EBADCSRFTOKEN') return next(err)
//     res.status(403)
//     res.send('Hacker ERROR')
// })


// Passport
const User = require('./www/model/User')
const pp = require('passport')
const bcrypt = require('bcrypt');
const Local = require('passport-local')
pp.serializeUser((user, done) => { done(null, user); });
pp.deserializeUser((user, done) => { done(null, user); });
pp.use(new Local({
	usernameField: 'username',
	passwordField: 'password'
}, (username, password, done) => {
	User.findOne({
		username: username,
	}).then(async (user) => {
		let passOk = await bcrypt.compare(password, user.password)
		if (passOk) {
			done(null, { username: user.username, level: user.level }); // Info zum User
		} else {
			done(null);
		}
		// 
	}).catch((e) => {
		console.log('falscher Username', e);
		// res.send({ status: false, name: 'Username nicht gefunden!' });
	})
}));

// passport.use(new OAuth2Strategy({
// 	authorizationURL: 'https://www.example.com/oauth2/authorize',
// 	tokenURL: 'https://www.example.com/oauth2/token',
// 	clientID: EXAMPLE_CLIENT_ID,
// 	clientSecret: EXAMPLE_CLIENT_SECRET,
// 	callbackURL: "http://localhost:3000/auth/example/callback"
// },
// 	function (accessToken, refreshToken, profile, cb) {
// 		User.findOrCreate({ exampleId: profile.id }, function (err, user) {
// 			return cb(err, user);
// 		});
// 	}
// ));


// Express-Session
const session = require('express-session')

app.use(session({
	secret: 'geheim',
	resave: false,
	saveUninitialized: false
}));

app.use(pp.initialize());
app.use(pp.session());


// Check Auth
const checkAuth = (req, res, next) => {
	if (req.user) {
		next();
	} else {
		res.redirect('/user/login');
	}
}


// Express-Middleware
app.use(express.json());
app.use(express.static('www/view'));

const appRouter = require('./www/routes')
app.use('/', appRouter)

const usermanager = require('./www/routes/user')
app.use('/user', usermanager)

const quizmanager = require('./www/routes/quiz');
app.use('/quiz', checkAuth, quizmanager)



// Error Handler
app.use((req, res, next) => {
	res.status(404).redirect('/error')
})
