require('dotenv').config()
var MongoClient = require('mongodb').MongoClient;
var url = process.env.MONGO_LINK;

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("mydb");
  var myobj = { name: "Sebastian", age: 22, destrict: 15, plz: 1150, hobbies: ['soccer', 'playing Guitar']};
  dbo.collection("customers").insertOne(myobj, function(err, res) {
    if (err) throw err;
    console.log("1 document inserted");
    db.close();
  });
});