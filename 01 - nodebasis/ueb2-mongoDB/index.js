require('dotenv').config()
const MongoClient = require('mongodb').MongoClient;

const URL = process.env.MONGO_LINK;
const DB = process.env.DB;
const COLLECTION = process.env.COLLECTION;

MongoClient.connect(URL, (err, db) => {
	if (err) throw err;
	const dbo = db.db(DB);
	dbo.collection(COLLECTION).findOne({}, (err, result) => {
		if (err) throw err;
		console.log(result);
		db.close();
	});
});