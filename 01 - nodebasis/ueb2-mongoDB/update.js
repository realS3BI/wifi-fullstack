require('dotenv').config()
var MongoClient = require('mongodb').MongoClient;
var url = process.env.MONGO_LINK;

MongoClient.connect(url, function (err, db) {
	if (err) throw err;
	var dbo = db.db("mydb");
	var myquery = { age: 22 };
	var newvalues = { $set: { age: 23 } };
	dbo.collection("customers").updateOne(myquery, newvalues, function (err, res) {
		if (err) throw err;
		console.log("1 document updated");
		db.close();
	});
});