const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  name: {
    type: String, required: [true, "username required"],
  },
  hobbies: {
    type: String, required: [true, "username required"],
  },
  country: {
    type: String, required: [true, "username required"],
  },
  hobby: {
    type: String, required: [true, "username required"],
  },
  favAnimal: {
    type: String, required: [true, "username required"],
  },
});

module.exports = mongoose.model("User", userSchema);
