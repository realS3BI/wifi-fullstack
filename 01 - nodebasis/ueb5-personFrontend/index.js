const express = require("express");
const app = express();
const User = require("./models/user");
const mongoose = require("mongoose");

mongoose
	.connect("mongodb://localhost:27017/loginDemo", {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	})
	.then(() => {
		console.log("connection open");
	})
	.catch((err) => {
		console.log(err);
	});

app.set("view engine", "ejs");
app.set("views", "views");

app.use(express.urlencoded({ extended: true }));


app.get("/person", (req, res) => {
	res.render("person");
});

app.post("/person", async (req, res) => {
	const newKey = req.body.newKey;
	const newValue = req.body.newValue;
	const user = new User({ name: newKey, hobby: newValue })
	console.log(user);
	// await user.save()
	// newTest
	// console.log(res.req.body);
	// const person = await User.findOne({ hobby })

	// const object = req.body
	// const 



	// const { username, password } = req.body;
	// const user = await User.findOne({ username });
	// const validPassword = await bcrypt.compare(password, user.password);
	// if (validPassword) {
	// 	res.send("Welcome");
	// } else {
	// 	res.send("try again ");
	// }
});

// app.post("/register", async (req, res) => {
// 	const { password, username } = req.body;
// 	const hash = await bcrypt.hash(password, 12);
// 	const user = new User({
// 		username,
// 		password: hash,
// 	});
// 	await user.save();
// 	res.redirect("/");
// });

// app.get("/secret", (req, res) => {
// 	res.send("Sichtbar für eingeloggte");
// });

app.listen(3000, () => {
	console.log("listending on port 3000");
});
