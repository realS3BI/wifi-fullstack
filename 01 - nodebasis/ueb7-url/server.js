require('dotenv').config();

const url = require('url');

const express = require('express');
const app = express();
const port = '3000'

const MongoClient = require('mongodb').MongoClient;

const URL = process.env.MONGO_LINK;
const DB = process.env.DB;
const COLLECTION = process.env.COLLECTION;

app.set('view engine', 'ejs');

app.get('/', function (req, res) {
	// var q = url.parse(req.url, true)

	if (JSON.stringify(req.query) == '{}') {
		return
	}

	// MongoClient.connect(URL, (err, db) => {
	// 	if (err) throw err;
	// 	const dbo = db.db(DB);
	// 	dbo.collection(COLLECTION).insertOne(req.query, function (err, res) {
	// 		if (err) throw err;
	// 		console.log("1 document inserted", req.query);
	// 		db.close();
	// 	});
	// });
	console.log(req.query);
	res.render('index.ejs')
});

app.listen(port, () => console.log('Server has started'));