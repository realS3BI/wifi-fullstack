const http = require('http');
const url = require('url');
const fs = require('fs');
const crypto = require('crypto');
require('dotenv').config()

const MongoClient = require('mongodb').MongoClient;

const URL = process.env.MONGO_LINK;
const DB = process.env.DB;
const COLLECTION = process.env.COLLECTION;

const hostname = '127.0.0.1';
const port = 3000;


let key = '12345678912345678912345678912345';
let iv = crypto.randomBytes(16);

const server = http.createServer((req, res) => {
	if (req.url != '/favicon.ico') {
		res.writeHead(200, { 'Content-Type': 'text/html' });
		const queryObject = JSON.stringify(url.parse(req.url, true).query)

		console.log(queryObject, 'Queryobject');

		let mykey = crypto.createCipheriv('aes-256-cbc', key, iv);
		let encrypted = mykey.update(queryObject, 'utf-8', 'hex')
		encrypted += mykey.final('hex')

		fs.writeFile(`test.txt`, encrypted, (err) => {
			getHash(res)
			if (err) {
				console.log(err, 'error');
			}
		})
		encrypted = JSON.stringify(encrypted)

		// let storage = localStorage.setItem('hashedQuery', encrypted)
		// console.log(storage);

		
	}
})

function getHash(res) {
	console.log('test');
	fs.readFile('test.txt', 'utf8', (err, data) => {
		if (err) {
			console.error(err)
			return
		}

		var mykey = crypto.createDecipheriv('aes-256-cbc', key, iv);
		var mystr = mykey.update(data, 'hex', 'utf8')
		mystr += mykey.final('utf8');

		console.log(mystr, 'encrypted');
		res.end(mystr)
	})
}

server.listen(port);

console.log(`Server running at http://${hostname}:${port}/`);