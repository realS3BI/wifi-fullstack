const http = require('http');
const myModule = require('./myModule.js')

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('Hello World');
});

server.listen(port);

console.log(myModule);

console.log(`Server running at http://${hostname}:${port}/`);