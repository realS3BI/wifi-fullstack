import "./App.css";
import React from "react";
import { BrowserRouter, Routes, Route, NavLink } from "react-router-dom";

import Map from "./Map";

export default class App extends React.Component {
	render() {
		return (
			<BrowserRouter>
				<div className="wrapper">
					<header>Orte</header>
					<nav>
						<NavLink to="/">Home</NavLink>
					</nav>
					<main>
						<Routes>
							<Route path="*" element={<Map />} />
						</Routes>
					</main>
				</div>
			</BrowserRouter>
		);
	}
}
