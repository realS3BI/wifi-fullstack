import React from "react";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import { v4 } from "uuid";

import "./Map.css";

export default class Map extends React.Component {
	state = {
		orte: [],
		startCoor: [48.21, 16.37],
		startZoom: 10,
		coorX: "",
		coorY: "",
		text: "",
	};
	handleSubmit(e) {
		e.preventDefault();
		this.setState({
			orte: [
				...this.state.orte,
				{
					id: v4(),
					coorX: this.state.coorX,
					coorY: this.state.coorY,
					text: this.state.text,
				},
			],
			startCoor: [this.state.coorX, this.state.coorY],
			coorX: "",
			coorY: "",
			text: "",
		});
	}

	handleInputChange(event) {
		const target = event.target;
		const value = target.value;
		const name = target.name;

		this.setState({
			[name]: value,
		});
		console.log(this.state);
	}

	render() {
		return (
			<>
				<form onSubmit={this.handleSubmit.bind(this)}>
					<input
						type="number"
						name="coorX"
						placeholder="Coordinate X"
						onChange={(e) => {
							this.handleInputChange(e);
						}}
					/>
					<input
						type="number"
						name="coorY"
						placeholder="Coordinate Y"
						onChange={(e) => {
							this.handleInputChange(e);
						}}
					/>
					<input
						type="text"
						name="text"
						placeholder="Text"
						onChange={(e) => {
							this.handleInputChange(e);
						}}
					/>
					<input type="submit" value="Add" />
				</form>
				<hr />
				<MapContainer
					center={this.state.startCoor}
					zoom={this.state.startZoom}
					className="map"
				>
					<TileLayer
						attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
						url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
					/>
					{this.state.orte.map((el) => (
						<Marker key={el.id} position={[el.coorX, el.coorY]}>
							<Popup>{el.text}</Popup>
						</Marker>
					))}
				</MapContainer>
			</>
		);
	}
}
