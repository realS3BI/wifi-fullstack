import React, {Component} from 'react';
import {BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';

import styles from './App.module.css';

import Home from './mailclient/pages/Home';
import Config from './mailclient/pages/Config';
import MailClient from './mailclient/pages/MailClient';
import MainNavigation from './shared/components/Navigation/MainNavigation';


class App extends Component {
  /*
  state = {
    emails:[]
  }

  componentDidMount() {
    fetch( 'http://localhost:5009/mails', {
        method:'get'
    } )
    .then( response => response.json() )
    .then( data => {
        //console.log( data );
        this.setState( {emails: data } );
    })
  } 
*/
  render() {
    return (
      <BrowserRouter>
        <div className={styles.wrapper}>
          <MainNavigation />
          <main>
            <Routes>
              <Route path="/" element={ <Home /> } />
              <Route path="/mails" element={ <MailClient /> } />{ /*emails={this.state.emails} */ }
              <Route path="/config" element={ <Config /> } />
              <Route path="*" element={ <Navigate to="/" replace /> } />
            </Routes>       
          </main>
          <footer className={styles.footer}>© WIFI MERN Developer 2022</footer>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
