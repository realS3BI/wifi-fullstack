import React, {Component} from 'react';

import styles from './EmailList.module.css';
import EmailListItem from './EmailListItem';

export default class EmailList extends Component {
    /*constructor( props ) {
        super( props );
    }*/
    render() {
        if (this.props.loading === true) {
            return (
                <div className={[styles.emaillist,styles.empty].join(' ')}>
                    <div className={styles.loader}></div>
                </div>
            );
        } 


        if (this.props.emails.length === 0) {
            return (
                <div className={[styles.emaillist,styles.empty].join(' ')}>
                    keine E-Mails vorhanden
                </div>
            );
        } 

        return (
            <div className={styles.emaillist}>
                {
                    this.props.emails.map( email => (
                        <EmailListItem 
                            email={email}
                            key={email.id}
                            onEmailClicked={ (id)=>{ this.props.onEmailSelected(id); } }
                            selected={this.props.selectedEmailId === email.id}
                        />
                    ))
                }
            </div>
        )
    }
}