import React, {Component} from 'react';

import styles from './EmailListItem.module.css';

import { getPrettyDate } from '../../shared/util/helpers';

export default class EmailListItem extends Component {
    constructor( props ) {
        super( props );
    }


    
    render() {

        let classes = [styles.emailitem];
        if (this.props.selected) {
            classes.push(styles.selected)
        }

        return (
            <div onClick={() => { this.props.onEmailClicked(this.props.email.id); }} className={classes.join(' ')}>
                <div className={styles.emailitem__unreaddot} data-read={this.props.email.read}></div>
                <div className={styles.emailitem__subject}>{this.props.email.subject}</div>
                <div className={styles.emailitem__details}>
                    <span className={styles.emailitem__from}>{this.props.email.from}</span>
                    <span className={styles.emailitem__time}>{getPrettyDate( this.props.email.time )}</span>
                </div>
            </div>
        )
    }
}