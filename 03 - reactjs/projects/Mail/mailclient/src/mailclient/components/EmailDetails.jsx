import React, {Component} from 'react';
import styles from './EmailDetails.module.css';


import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { getPrettyDate } from '../../shared/util/helpers';

export default class EmailDetails extends Component {
    
    render() {
        if (!this.props.email) {
            return (
                <div className={[styles.emailcontent, styles.empty].join(' ')}></div>
            );
        }
        return (
            <div className={styles.emailcontent}>
                <div className={styles.emailcontent__header}>
                    <h3 className={styles.emailcontent__subject}>{this.props.email.subject}</h3>
                    {this.props.email.tag !== 'trash'&&
                        <span 
                            onClick={() => { this.props.onDelete(this.props.email.id); }} 
                            className={styles.deletebtn}>
                                <FontAwesomeIcon icon={faTrash} />
                        </span>
                    }
                    <div className={styles.emailcontent__time}>{getPrettyDate(this.props.email.time)}</div>
                    <div className={styles.emailcontent__from}>{this.props.email.from}</div>
                </div>
                <div className={styles.emailcontent__message}>{this.props.email.text}</div>
            </div>
        )
    }
}