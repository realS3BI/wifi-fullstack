import React, {Component} from 'react';
import styles from './Sidebar.module.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInbox, faPencil, faPaperPlane, faPencilSquare, faTrash } from '@fortawesome/free-solid-svg-icons';

import {ThemeContext} from '../../shared/context/theme-context.js';

export default class Sidebar extends Component {

    static contextType = ThemeContext;

    changeSelection(e) {
        e.preventDefault();
        this.props.setSelection( e.target.getAttribute('data-tag') );
    }

    unreadCount() { 
        return this.props.emails.reduce(
          (previous, msg) => {
             if (msg.read !== "true" && msg.tag==='inbox' ) {
                 return previous + 1;
             }
             else {
                 return previous;
             }
         }, 0) ;
     }
 
     tagCount(tag) { return this.props.emails.reduce(
         function(previous, msg) {
             if (msg.tag === tag) {
                 return previous + 1;
             }
             else {
                 return previous;
             }
         }, 0);
     }
 

    render() {
        let themeObject = this.context;
        return (
            <div className={styles.sidebar} style={{backgroundColor:themeObject.theme==='standard' ? '#34393d':'#0000FF'}}>
                <div className={styles.sidebar__compose}>
                    <a href="/" className={styles.btncompose}>
                        Neue Nachricht <FontAwesomeIcon icon={faPencil} />
                    </a>
                </div>
                <ul className={styles.sidebar__inboxes}>
                    <li>
                        <a href="/" onClick={this.changeSelection.bind(this)} data-tag="inbox">
                            <FontAwesomeIcon icon={faInbox} />
                            Posteingang
                            <span className={styles.itemcount}>{this.unreadCount()}</span>
                        </a>
                    </li>
                    <li>
                        <a href="/" onClick={this.changeSelection.bind(this)} data-tag="sent">
                            <FontAwesomeIcon icon={faPaperPlane} />Gesendete
                            <span className={styles.itemcount}>{this.tagCount('sent')}</span>
                        </a>
                    </li>
                    <li>
                        <a href="/" onClick={this.changeSelection.bind(this)} data-tag="draft">
                            <FontAwesomeIcon icon={faPencilSquare} />Entwürfe
                            <span className={styles.itemcount}>{this.tagCount('draft')}</span>
                        </a>
                    </li>
                    <li>
                        <a href="/" onClick={this.changeSelection.bind(this)} data-tag="trash">
                            <FontAwesomeIcon icon={faTrash} />Gelöschte
                            <span className={styles.itemcount}>{this.tagCount('trash')}</span>
                        </a>
                    </li>
                </ul>
            </div>
        )
    }
}