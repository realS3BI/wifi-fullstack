import React, {Component} from 'react';
import styles from './Home.module.css';

import Title from '../../shared/components/UIElements/Title';
import Red from '../../shared/components/UIElements/Red';
import LoginForm from './LoginForm';


import {ThemeContext} from '../../shared/context/theme-context.js';

export default class Home extends Component {    
    static contextType = ThemeContext;
    render() {
        return (
            <div className={styles.maincontent}>
                <Title>Will<Red>kommen</Red>{this.context.username}</Title>
                <hr />
                <LoginForm />
            </div>
        )
    }
}