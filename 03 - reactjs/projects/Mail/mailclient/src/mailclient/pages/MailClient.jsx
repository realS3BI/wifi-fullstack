import React, {Component} from 'react';
import styles from './MailClient.module.css';

import Sidebar from '../components/Sidebar';
import EmailList from '../components/EmailList';
import EmailDetails from '../components/EmailDetails';


export default class MailClient extends Component {

    state = {
        selectedEmailId: 0, /* Auswahl E-Mail Details */
        currentSelection: 'inbox', /* Auswahl Sidebar */ 
        emails:[ /* Daten...*/ ],
        loading: true,
    }

    constructor( props ) {
        super(props);
        //this.state.emails = this.props.emails;
    }

    /*
    static getDerivedStateFromProps( props,state ) {
        state.emails = props.emails;
    }
    */

    componentDidMount() {
        fetch( 'http://localhost:5009/mails', {
            method:'get'
        } )
        .then( response => response.json() )
        .then( data => {
            //console.log( data );
            this.setState( {emails: data, loading: false } );
        })
    } 

    setSelection(tag) {
      
		let selectedEmailId = this.state.selectedEmailId;
		if (tag !== this.state.currentSelection) {
			selectedEmailId = 0;
		}	
		this.setState({
			currentSelection: tag,
			selectedEmailId
		});
	}

    openEmail( id ) {       
	    const emails = this.state.emails;
		const index = emails.findIndex(x => x.id === id);		
        emails[index].read = 'true';
		this.setState({selectedEmailId: id, emails });
    }

    deleteMessage(id) {
        const emails = this.state.emails;
		const index = emails.findIndex(x => x.id === id);
		emails[index].tag = 'trash';
		
		let selectedEmailId = 0;
       
		for (const email of emails) {
			if (email.tag === this.state.currentSelection) {
				selectedEmailId = email.id;	                
                email.read = 'true';    
				break;
			}
		}
		
		this.setState({
			emails,
			selectedEmailId
		});   
    }

    render() {
        return (
            <>
                <Sidebar 
                    emails={this.state.emails}
                    setSelection={ (tag)=>{ this.setSelection(tag); } }
                />
                <div className={styles.inboxcontainer}>
                    <EmailList 
                        loading={this.state.loading}
                        emails={this.state.emails.filter(email => email.tag === this.state.currentSelection )} 
                        onEmailSelected={(id) => { this.openEmail(id); }}
                        selectedEmailId={this.state.selectedEmailId}
                    />
                    <EmailDetails 
                        email={this.state.emails.find(email => email.id === this.state.selectedEmailId)}
						onDelete={(id) => { this.deleteMessage(id); }}                    
                    />
                </div>            
            </>
        )
    }
}