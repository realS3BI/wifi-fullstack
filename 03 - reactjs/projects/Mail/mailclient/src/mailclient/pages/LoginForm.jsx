import React, { Component } from "react";
// import {ThemeContext} from '../../context/theme-context.js';
import {ThemeContext} from '../../shared/context/theme-context';

export default class LoginForm extends Component {
	state = {
		username: "",
		password: "",
	};

	constructor(props) {
		super(props);
		this.handleInputChange = this.handleInputChange.bind(this);
	}
	encodeFormData(data) {
		return Object.keys(data)
			.map(
				(key) =>
					encodeURIComponent(key) + "=" + encodeURIComponent(data[key])
			)
			.join("&");
	}

	clickHandler(e) {
		console.log(this.state);
		e.preventDefault();
		fetch("http://localhost:5009/login", {
			headers: {
				"Content-Type": "application/x-www-form-urlencoded",
			},
			method: "POST",
			body: new URLSearchParams(this.state)
		})
			.then((res) => res.json())
			.then((result) => {
				console.log(result);
			});
	}

	handleInputChange(e) {
		const value = e.target.value;
		const name = e.target.name;

		this.setState({
			[name]: value,
		});
	}

	render() {
		return (
			<form method="post">
				Username:{" "}
				<input
					type="text"
					name="username"
					onChange={this.handleInputChange}
					value={this.state.username}
				/>
				<br />
				Passwort:{" "}
				<input
					type="password"
					name="password"
					onChange={this.handleInputChange}
					value={this.state.password}
				/>
				<br />
				<button onClick={(e) => this.clickHandler(e)}>Login</button>
			</form>
		);
	}
}
