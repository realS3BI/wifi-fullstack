import React, {Component} from 'react';
import styles from './Config.module.css';


import ThemeButtons from '../../shared/components/UIElements/ThemeButtons';

export default class Config extends Component {



    render() {
        return (
            <div className={styles.maincontent}>
                <h1>Konfiguration/Einstellungen</h1>
                <ThemeButtons />                
            </div>
        )
    }
}