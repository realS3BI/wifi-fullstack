import React, {Component} from 'react';
import styles from './MainNavigation.module.css';

import NavLinks from './NavLinks';
import {ThemeContext} from '../../context/theme-context.js';

export default class MainNavigation extends Component {

    static contextType = ThemeContext;

    render() {
        let themeObject = this.context;
        return (
          <nav className={styles.mainnav} style={{backgroundColor:themeObject.theme==='standard' ? '#34393d':'#0000FF'}}>
              {/* Logo, etc. */}
              <NavLinks />
          </nav> 
        )
    }
}

//MainNavigation.contextType = ThemeContext;