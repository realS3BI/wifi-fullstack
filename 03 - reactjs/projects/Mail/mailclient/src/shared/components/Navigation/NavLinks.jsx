import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';

import styles from './NavLinks.module.css';

export default class NavLinks extends Component {

    constructor() {
        super();
        this.hyperlinks = [
            {url:'/', text:'Home'},
            {url:'/mails', text:'E-Mails'},
            {url:'/config', text:'Einstellungen'},
            {url:'/test', text:'Test'}
        ];
    }

    createListItems() {
        return (
            this.hyperlinks.map( link=> (
                <li  key={Math.random()}><NavLink 
                   
                    className={(navData)=> navData.isActive ? styles.aktiviert : ''} 
                    to={link.url}>
                    {link.text}
                    </NavLink></li>
            ))

        );
    }

    render() {
        return (
          <ul className={styles.navlinks}>
            {this.createListItems()}  
           {/*   <li><NavLink className={(navData)=> navData.isActive ? styles.aktiviert : ''} to="/">Home</NavLink></li>
              <li><NavLink className={(navData)=> navData.isActive ? styles.aktiviert : ''} to="/mails">E-Mails</NavLink></li>
              <li><NavLink className={(navData)=> navData.isActive ? styles.aktiviert : ''} to="/config">Einstellungen</NavLink></li>
          */ }
          </ul>
          
        )
    }
}