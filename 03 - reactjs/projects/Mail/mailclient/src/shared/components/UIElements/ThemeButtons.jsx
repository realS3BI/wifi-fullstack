import React, {Component} from 'react';
import {ThemeContext} from '../../context/theme-context.js';

export default class ThemeButtons extends Component {
    //static contextType = ThemeContext;
    
    state = {
        theme:'standard'
    }

    render() {
        return (
            <ThemeContext.Provider value={this.state}>
                <button onClick={()=>{ this.setState({theme:'standard'})}}>Standard</button>
                <button onClick={()=>{ this.setState({theme:'blue'})}}>Blue</button>
            </ThemeContext.Provider>
        )
    }
}