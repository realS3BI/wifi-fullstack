import React, {Component} from 'react';

export default class Red extends Component {
    render() {
        return (
            <span style={{color:'red'}}>{this.props.children}</span>
        )
    }
}