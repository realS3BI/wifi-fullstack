import React, {Component} from 'react';
import styles from './Title.module.css';

export default class Title extends Component {
    render() {
        return (
            <h1 className={styles.title}>{this.props.children}</h1>
        )
    }
}