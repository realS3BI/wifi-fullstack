import { createContext } from "react";

export const ThemeContext = createContext( {
    theme: 'standard',
    isLoggedIn : false,
    token: '',
    username: 'Alex'
} );