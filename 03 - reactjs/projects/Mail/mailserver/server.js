const express = require('express');
const app = express();
const server = app.listen(5009);
const cors = require('cors');
app.use(cors());

app.get('/mails', (req, res) => {
	setTimeout(() => {
		res.sendFile(__dirname + '/mails.json');
	}, 3000);
})

app.use(express.urlencoded({ extended: false }));

app.post('/login', (req, res) => {
	console.log(req.body);
	if (req.body.username == 'admin' && req.body.password == '12345') {
		res.send(true);
	} else {
		res.send(false);
	}
})
