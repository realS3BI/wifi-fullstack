import {
	BrowserRouter,
	Routes,
	Route,
	Navigate,
	NavLink,
} from "react-router-dom";

import MovieList from "./MovieList";
import MovieDetails from "./MovieDetails";

const App = () => {
	return (
		<BrowserRouter>
			<header>
				<NavLink to="">Übersicht</NavLink>
				<NavLink to="/details">Details</NavLink>
			</header>
			<main>
				<Routes>
					<Route path="/" element={<MovieList />}></Route>
					<Route path="/detail" element={<MovieDetails />}></Route>
					<Route path="*" element={<Navigate />}></Route>
				</Routes>
			</main>
		</BrowserRouter>
	);
};

export default App;
