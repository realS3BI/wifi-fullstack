import { useState, useEffect } from "react";

const TMDB_API_URL = process.env.TMDB_API_URL;

console.log(TMDB_API_URL, process.env.TMDB_API_URL);

const loadMovies = () => {
	fetch(TMDB_API_URL, {})
		.then((res) => res.json())
		.then((data) => {
			console.log(data);
		});
};

function MovieList(props) {
	const [movies, setMovies] = useState([]);

	useEffect(loadMovies);

	return <>MovieListe</>;
}

export default MovieList;
