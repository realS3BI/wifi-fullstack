import "./App.css";
import TodoForm from "./TodoForm";
import TodoListe from "./TodoListe";
import React, { useState, useEffect } from "react";

import { v4 } from "uuid";

function App() {
	let [todos, setTodos] = useState([]);

	let savedTodos = localStorage.getItem("reactTodos");
	if (savedTodos) {
		todos = JSON.parse(savedTodos);
	}

	const toggleChecked = (id) => {
		console.log("toggleTODO", id);
		const updatedTodos = todos.map((task) =>
			id === task.id ? { ...task, checked: !task.checked } : task
		);
		saveTodos(updatedTodos);
	};

	const saveTodos = (todos) => {
		localStorage.setItem("reactTodos", JSON.stringify(todos));
		setTodos(todos);
	};

	const addTodo = (title) => {
		let newTodo = {
			id: v4(),
			title: title,
			checked: false,
		};
		todos.push(newTodo);
		saveTodos(todos);
	};

	const deleteTodo = (id) => {
		const updatedTodos = todos.filter((task) => id !== task.id);
		saveTodos(updatedTodos);
	};

	return (
		<div className="todoliste">
			<TodoForm addTodo={addTodo} />
			<TodoListe
				tasks={todos}
				toggleChecked={toggleChecked}
				deleteTodo={deleteTodo}
			/>
		</div>
	);
}

export default App;
