import React from "react";
import "./Todo.css";

function Todo(props) {
	const clickHandler = () => {
		props.toggleChecked(props.id);
	};

	const clickHandlerDelete = (e) => {
		e.stopPropagation();
		props.deleteTodo(props.id);
	};

	console.log("render");
	return (
		<li
			className={props.checked ? "checked" : ""}
			onClick={() => {
				clickHandler();
			}}
		>
			{props.title}
			<span className="close" onClick={(e) => clickHandlerDelete(e)}>
				&#x00D7;
			</span>
		</li>
	);
}

export default Todo;
