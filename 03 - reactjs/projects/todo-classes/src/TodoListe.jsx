import React from "react";
import "./TodoListe.css";
import Todo from "./Todo";

function TodoListe(props) {
	console.log("render TodoListe");
	return (
		<ul id="myUL">
			{props.tasks.map((el) => (
				<Todo
					title={el.title}
					checked={el.checked}
					id={el.id}
					key={el.id}
					toggleChecked={props.toggleChecked}
					deleteTodo={props.deleteTodo}
				/>
			))}
		</ul>
	);
}

export default TodoListe;
