import React, { useState, useRef } from "react";
import "./TodoForm.css";

function TodoForm(props) {
	const [newTodoValue, setNewTodoValue] = useState('');

	const textInput = useRef(null);

	const handleClick = (e) => {
		e.preventDefault();
		if (newTodoValue !== '') {
			props.addTodo(newTodoValue);
			setNewTodoValue('');
			textInput.current.focus();
		}
	};

	const handleChange = (e) => {
		setNewTodoValue(e.target.value);
	};

	return (
		<form>
			<h2>My To Do List</h2>
			<input
				type="text"
				placeholder="Title..."
				value={newTodoValue}
				onChange={(e) => {
					handleChange(e);
				}}
				autoFocus
				ref={textInput}
			/>
			<button className="addBtn" onClick={(e) => handleClick(e)}>
				Add
			</button>
		</form>
	);
}

export default TodoForm;
